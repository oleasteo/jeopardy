import {compact, filter, map} from "lodash";
import game from "../../data/game";

/*===================================================== Exports  =====================================================*/

export {getDisplayText, discardField, setFieldValue, resolveField, invalidatePlayer, getFallback, getAvailableFields};

/*==================================================== Functions  ====================================================*/

function getFallback() {
  return {
    active: randomPlayer(game),
    score: map(game.categories, (category) => {
      return map(category.scores || game.scores, (scoreData) => scoreData == null ? null : stubField());
    }),
  };
}

function stubField() {
  return {
    done: false,
    failed: [/*{player,value}*/],
    player: null,
    value: null,
  };
}

function setFieldValue({status}, categoryIndex, scoreIndex, scoreValue) {
  status.score[categoryIndex][scoreIndex].value = scoreValue;
}

function discardField({game, status}, categoryIndex, scoreIndex) {
  status.active = randomPlayer(game);
  status.score[categoryIndex][scoreIndex].done = true;
}

function resolveField({status}, categoryIndex, scoreIndex, playerIndex) {
  status.active = playerIndex;
  status.score[categoryIndex][scoreIndex].player = playerIndex;
  status.score[categoryIndex][scoreIndex].done = true;
}

function invalidatePlayer({status}, categoryIndex, scoreIndex, playerIndex, scoreValue) {
  status.score[categoryIndex][scoreIndex].failed.push({
    player: playerIndex,
    value: scoreValue,
  });
}

function getDisplayText(fieldScore, scoreData) {
  if (fieldScore && fieldScore.value != null) { return "" + fieldScore.value; }
  if (scoreData == null) { return ""; }
  if (typeof scoreData === "number") { return "" + scoreData; }
  if (typeof scoreData.value === "number") { return "" + scoreData.value; }
  return "" + scoreData.value.display;
}

function getAvailableFields({game, status}) {
  return compact(map(game.categories, (category, categoryIndex) => {
    const scoreIndices = Object.keys(category.scores || game.scores);
    const availableScoreIndices = filter(scoreIndices, (scoreIndex) => {
      const scoreData = status.score[categoryIndex][scoreIndex];
      return scoreData != null && !scoreData.done;
    });
    if (!availableScoreIndices.length) { return null; }
    return {
      categoryIndex,
      scoreIndices: availableScoreIndices,
    };
  }));
}

function randomPlayer(game) { return Math.floor(Math.random() * game.players.length); }
