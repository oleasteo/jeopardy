import {each, map, max, sortBy, times, toPairs} from "lodash";

import chalk from "./chalk";
import {print as printTable} from "./table";
import {numberWidth, repeat} from "../util/pad";
import config from "../../config";
import {getDisplayText as getScoreDisplayText} from "../game/status";

const GAME_META = new WeakMap();

/*===================================================== Exports  =====================================================*/

export {print, printPlayerName};

/*==================================================== Functions  ====================================================*/

function eachField({game}, cb) {
  for (let scoreIdx = 0, hasNext = true; hasNext; scoreIdx++) {
    hasNext = false;
    for (let categoryIdx = 0; categoryIdx < game.categories.length; categoryIdx++) {
      const category = game.categories[categoryIdx];
      const categoryScores = category.scores || game.scores;
      cb({category, categoryScore: categoryScores[scoreIdx], categoryIdx, scoreIdx});
      if (categoryScores.length - 1 > scoreIdx) { hasNext = true; }
    }
  }
}

function printPlayerName(data, playerIndex) {
  getMeta(data).pencil.players[playerIndex](data.game.players[playerIndex]);
}

function print(data) {
  const {game, status} = data;
  const meta = getMeta(data);
  const playerScores = times(game.players.length, () => 0);
  const playerPenalties = times(game.players.length, () => 0);
  meta.pencil.default.clear();
  // collect data
  const header = map(game.categories, ({name}) => ({pencil: meta.pencil.default, lines: [name]}));
  const rows = [];
  eachField(data, ({categoryScore, categoryIdx, scoreIdx}) => {
    if (rows[scoreIdx] == null) { rows[scoreIdx] = []; }
    const fieldScore = status.score[categoryIdx][scoreIdx];
    const fieldScoreText = getScoreDisplayText(fieldScore, categoryScore);
    if (fieldScore != null) {
      each(fieldScore.failed, (fail) => {
        playerScores[fail.player] -= fail.value;
        playerPenalties[fail.player] += fail.value;
      });
      if (fieldScore.player != null) { playerScores[fieldScore.player] += fieldScore.value; }
    }
    rows[scoreIdx][categoryIdx] = {
      pencil: getFieldPencil(meta, fieldScore),
      lines: [
        fieldScoreText && "[" + fieldScoreText + "]",
        getFieldPlayerText(game, meta, fieldScore),
      ],
    };
  });
  // print table
  printTable(meta.pencil.default, meta.colWidth, meta.separator.normal, meta.separator.bold, header, rows);
  // print scores
  const scorePairs = sortBy(toPairs(playerScores), ([, score]) => -score);
  const scoreWidth = max(map(scorePairs, ([, score]) => ("" + score).length));
  each(scorePairs, ([playerIndex, scoreValue]) => {
    meta.pencil.default(repeat(" ", scoreWidth - ("" + scoreValue).length) + scoreValue + ": ");
    printPlayerName(data, playerIndex);
    const penalty = playerPenalties[playerIndex];
    if (penalty !== 0) { meta.pencil.default(` (${scoreValue + penalty} - ${penalty})`); }
    meta.pencil.default("\n");
  });
}

function getFieldPencil({pencil}, fieldScore) {
  if (fieldScore == null) { return pencil.default; }
  if (!fieldScore.done) { return pencil.available; }
  if (fieldScore.player == null) { return pencil.discarded; }
  return pencil.players[fieldScore.player];
}

function getFieldPlayerText(game, meta, fieldScore) {
  if (fieldScore == null || !fieldScore.done) { return ""; }
  return fieldScore.player == null ? "---" : game.players[fieldScore.player];
}

function getScoreDisplayWidth(score) {
  if (score == null) { return 0; }
  return max([
    score.value == null ? 0 : numberWidth(score.value) + 2,
    score.min == null ? 0 : numberWidth(score.min) + 2,
    score.max == null ? 0 : numberWidth(score.max) + 2,
  ]);
}

function getMeta(data) {
  if (GAME_META.has(data)) { return GAME_META.get(data); }
  const {game} = data;
  const colWidth = max([
    max(map(game.scores, getScoreDisplayWidth)),
    max(map(game.categories, ({name, scores}) => max([name.length, getScoreDisplayWidth(scores)]))),
    max(map(game.players, "length")),
  ]);
  const normalSeparator = times(game.categories.length, () => repeat("-", colWidth + 2)).join("|") + "\n";
  const boldSeparator = times(game.categories.length, () => repeat("=", colWidth + 2)).join("|") + "\n";
  const pencil = {
    default: chalk(config.chalk.default),
    available: chalk(config.chalk.available),
    discarded: chalk(config.chalk.discarded),
    players: map(game.players, (ignored, index) => chalk(config.chalk.players && config.chalk.players[index])),
  };
  GAME_META.set(data, {
    pencil,
    colWidth,
    separator: {normal: normalSeparator, bold: boldSeparator},
  });
  return GAME_META.get(data);
}
