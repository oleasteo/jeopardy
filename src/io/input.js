import {map} from "lodash";
import {terminal} from "terminal-kit";

import config from "../../config";
import {defChalk} from "./chalk";
import {getDisplayText as getScoreDisplayText} from "../game/status";
import {matches} from "./keys";

/*===================================================== Exports  =====================================================*/

export {fieldSelect, getScoreValue, firstPlayerChar, isCorrect};

/*==================================================== Functions  ====================================================*/

async function getScoreValue({status, game}, categoryIndex, scoreIndex) {
  const fieldScore = status.score[categoryIndex][scoreIndex];
  if (fieldScore && fieldScore.value != null) { return fieldScore.value; }
  const scoreData = (game.categories[categoryIndex].scores || game.scores)[scoreIndex];
  if (scoreData == null) { return null; }
  if (typeof scoreData === "number") { return scoreData; }
  if (typeof scoreData.value === "number") { return scoreData.value; }
  const scoreRange = scoreData.value;
  if (typeof scoreRange.max === "number" || typeof scoreRange.min === "number") {
    const min = scoreRange.min == null ? scoreRange.display : scoreRange.min;
    const max = scoreRange.max == null ? scoreRange.display : scoreRange.max;
    defChalk("Choose value between " + min + " and " + max + ": ");
    return await new Promise((resolve, reject) => {
      terminal.inputField({default: scoreRange.display}, (err, result) => {
        if (err != null) { return reject(err); }
        // todo check for out-of-bounds input
        resolve(Number.isNaN(+result) ? scoreRange.display : +result);
      });
    });
  }
  return scoreRange.value.display;
}

async function fieldSelect({game}, availableFields) {
  let inputIndex;
  const categoryValues = map(availableFields, ({categoryIndex}) => game.categories[categoryIndex].name);
  inputIndex = await singleColumnMenu(categoryValues);
  const field = availableFields[inputIndex];
  const scores = game.categories[field.categoryIndex].scores || game.scores;
  const scoreValues = map(field.scoreIndices, (scoreIndex) => getScoreDisplayText(null, scores[scoreIndex]));
  inputIndex = await singleColumnMenu(scoreValues);
  return {categoryIndex: field.categoryIndex, scoreIndex: field.scoreIndices[inputIndex]};
}

async function firstPlayerChar({game}) {
  terminal.bold("READY FOR KEY PRESS ");
  return await new Promise((resolve) => {
    terminal.on("key", listener);

    function listener(name, ignored, data) {
      if (matches(config.keys.discard, name, ignored, data)) {
        terminal.off("key", listener);
        return resolve(null);
      }
      for (let i = 0; i < game.players.length; i++) {
        if (matches(config.keys.players[i], name, ignored, data)) {
          terminal.off("key", listener);
          return resolve(i);
        }
      }
    }
  });
}

async function isCorrect(value) {
  const idx = await singleColumnMenu(["Incorrect", "Correct", "Nullify", "Custom Value"]);
  if (idx === 3) { // Custom Value
    const res = await numberInput(value);
    return {valid: value === 0 ? res >= 0 : res > 0, value: res};
  }
  return {
    valid: idx !== 0,
    nullify: idx === 2,
    value,
  };
}

async function singleColumnMenu(values, options) {
  return await new Promise((resolve, reject) => {
    terminal.singleColumnMenu(values, options, (err, data) => {
      if (err != null) { return reject(err); }
      resolve(data.selectedIndex);
    });
  });
}

async function numberInput(value) {
  return await new Promise((resolve, reject) => {
    terminal.inputField({default: value}, (err, result) => {
      if (err != null) { return reject(err); }
      resolve(Number.isNaN(+result) ? value : +result);
    });
  });
}
