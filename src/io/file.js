import fs from "fs";

/*===================================================== Exports  =====================================================*/

export {readJSON, writeJSON};

/*==================================================== Functions  ====================================================*/

async function readJSON(file, fallback) {
  return await new Promise((resolve, reject) => {
    return fs.access(file, fs.R_OK, (err) => {
      if (err != null) {
        if (fallback == null) { return reject(err); }
        return resolve(typeof fallback === "function" ? fallback() : fallback);
      }
      fs.readFile(file, (err, content) => {
        if (err != null) { return reject(err); }
        try { resolve(JSON.parse(content)); } catch (err) { reject(err); }
      });
    });
  });
}

async function writeJSON(file, data) {
  return await new Promise((resolve, reject) => {
    fs.writeFile(file, JSON.stringify(data, null, 2), (err) => {
      if (err != null) { return reject(err); }
      resolve();
    });
  });
}
