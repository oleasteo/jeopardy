export {repeat, center, numberWidth};

function repeat(char, times) {
  let c = "";
  while (c.length < times) { c += char; }
  return c;
}

function center(str, width, pad = " ") {
  const padSize = (width - str.length) / 2;
  return repeat(pad, Math.floor(padSize)) + str + repeat(pad, Math.ceil(padSize));
}

function numberWidth(num) { return Math.ceil(Math.log10(num < 0 ? 10 * -num : num)); }
