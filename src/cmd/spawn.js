import path from "path";
import {assign} from "lodash";
import {spawn as spawnChild} from "child_process";

import {kill as terminate} from "./kill";
import config from "../../config";

const COUNTDOWN = config.countdown && assign({}, config.countdown, {
  file: config.countdown.file ? path.join(__dirname, "..", "..", config.countdown.file) : null,
});

let spawned = [];

/*===================================================== Exports  =====================================================*/

export {spawn, countdown, shutdown};

/*==================================================== Functions  ====================================================*/

async function shutdown() {
  const list = spawned;
  spawned = [];
  await Promise.all(list.map((s) => s.stop("SIGTERM")));
}

function countdown() { return COUNTDOWN && spawn(COUNTDOWN); }

function spawn(data) {
  const pData = {
    child: null,
    exited: false,

    exitListener() { this.exited = true; },

    async stop(signal) {
      if (this.exited) { return; }
      this.exited = true;
      return new Promise((resolve) => {
        this.child.on("exit", () => resolve());
        terminate(this.child.pid, signal || data.kill);
      });
    },

    async restart() {
      await this.stop();
      this.start();
    },

    start() {
      this.child = open(data);
      this.exited = false;
      this.child.on("exit", this.exitListener.bind(this));
    },
  };

  pData.start();
  spawned.push(pData);
  return pData;
}

function open({file, cmd, argv}) {
  if (argv == null) { argv = ["%F"]; }
  return spawnChild(cmd || "xdg-open", argv.map((arg) => arg.replace("%F", file)));
}
